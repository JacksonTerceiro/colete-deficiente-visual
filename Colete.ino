#include <Ultrasonic.h>

#define Frente_Trig 8
#define Frente_Echo 9
Ultrasonic Frente(Frente_Trig, Frente_Echo);
float Frente_val = 0;
long microsecF = 0;

#define Esquerda_Trig 10
#define Esquerda_Echo 11
Ultrasonic Esquerda(Esquerda_Trig, Esquerda_Echo);
float Esquerda_val = 0;
long microsecE = 0;

#define Direita_Trig 12
#define Direita_Echo 13
Ultrasonic Direita(Direita_Trig, Direita_Echo);
float Direita_val = 0;
long microsecD = 0;

#define OFF HIGH
#define ON LOW
#define LIGA HIGH
#define DESLIGA LOW

/* Constante com valor de 50 cm - Controle da distancia de detecção dos obstaculos*/
#define DISTANCIA_OBS 50

/* Constantes para controle de motores */ 
#define engineLeft 6 //Constante de definicao do pino de controle do motor esquerdo - Pino 6
#define engineRight 7 //Constante de definicao do pino de controle do motor direito - Pino 7

/* Constantes para controle de luzes */
#define RedLeft 2 //luz vermelha esquerda - pino 2
#define RedRight 3 //luz vermelha direita - pino 3
#define BlueLeft 5 //luz azul esquerda - pino 5 
#define BlueRight 4 //luz azul direita - pino 4

/* funcoes de controle de motores _L - Liga, _D - Desliga */
void EngineLeftOn(){ 
  digitalWrite(engineLeft, LIGA);
}

void EngineLeftOFF(){
  digitalWrite(engineLeft, DESLIGA);
}

void EngineRightOn(){
  digitalWrite(engineRight, LIGA);
}

void EngineRightOFF(){
  digitalWrite(engineRight, DESLIGA);
}

/* funcoes de controle de luzes _L - Liga, _D - Desliga */
void RedLeftON(){
  digitalWrite(RedLeft, ON);
}

void RedRightON(){
  digitalWrite(RedRight, ON);
}

void BlueLeftON(){
  digitalWrite(BlueLeft, ON);
}

void BlueRightON(){
  digitalWrite(BlueRight, ON);
}

void RedLeftOFF(){
  digitalWrite(RedLeft, OFF);
}

void RedRightOFF(){
  digitalWrite(RedRight, OFF);
}

void BlueLeftOFF(){
  digitalWrite(BlueLeft,OFF);
}

void BlueRightOFF(){
  digitalWrite(BlueRight,OFF);
}


void setup() {
  
  /* para leitura de muitos sensores foi preciso aumentar o baudrate para 57600 afim de garantir a leitura adequada */
  Serial.begin(57600);
  
  /* informando atuadores */
  pinMode(engineLeft, OUTPUT);
  pinMode(engineRight, OUTPUT);
  pinMode(BlueLeft, OUTPUT); 
  pinMode(RedLeft, OUTPUT);
  pinMode(RedRight, OUTPUT);
  pinMode(BlueRight, OUTPUT);
  
}

void loop() {
  
  while(true){//garantia de DEUS que vai ter um loop infinito

    /* desligando todas as lampadas a cada iteração */
    BlueLeftOFF();
    BlueRightOFF();
    RedLeftOFF();
    RedRightOFF();
    
    microsecF = Frente.timing(); /* Leitura do sensor FRENTE */
    Frente_val = Frente.convert(microsecF, Ultrasonic::CM); /* Leitura do sensor FRENTE */

#ifdef DEBUG
    Serial.print("Frente: ");
    Serial.println(Frente_val);
#endif //verificar se funciona!

    if(Frente_val <= 50){ //criar define com o valor 50cm como constante
      Serial.print("!! Parede !!");
      RedLeftON();
      RedRightON();
      EngineLeftOn();
      EngineRightOn();
      delay(3000);
      EngineLeftOFF();
      EngineRightOFF();


      microsecE = Esquerda.timing(); /* Leitura do sensor ESQUERDA */
      microsecD = Direita.timing(); /* Leitura do sensor DIREITA */

      Esquerda_val = Esquerda.convert(microsecE, Ultrasonic::CM); /* Leitura do sensor ESQUERDA */
      Direita_val = Direita.convert(microsecD, Ultrasonic::CM); /* Leitura do sensor DIREITA */

      RedLeftOFF();
      RedRightOFF();

      //Serial.print("Direita: "); 
      //Serial.println(Direita_val);

       //PARA MOTORES


      if ((Esquerda_val > 50) && (Direita_val > 50)){
        //GIRA ESQUERDA E DIRETA 5 VEZES //DEPOIS PARA. (FOR)
        for(int x = 0; x < 6; x++){
          BlueLeftON();
          BlueRightOFF();
          EngineRightOFF();
          EngineLeftOn();
          delay(500);
          BlueLeftOFF();
          BlueRightON();
          EngineLeftOFF();
          EngineRightOn();
          delay(500);
        }

        BlueLeftOFF();
        BlueRightOFF();
        EngineLeftOFF();
        EngineRightOFF();
      }

      else if(Esquerda_val > 50){
        EngineLeftOn();
        RedRightON();
        for(int x = 0; x < 6; x++){
          BlueLeftON();
          delay(500);
          BlueLeftOFF();
          delay(500);
        }
        EngineLeftOFF();
        BlueLeftOFF();
        RedRightOFF();
        //GIRA ESQUERDA POR 5 SEGUNDOS E PARA. 
      }

      else if(Direita_val > 50){
        EngineRightOn();
        RedLeftON();
        for(int x = 0; x < 6; x++){
          BlueRightON();
          delay(500);
          BlueRightOFF();
          delay(500);
        }
        EngineRightOFF();
        BlueRightOFF();
        RedLeftOFF();
        //GIRA DIREITA POR 5 SEGUNDOS E PARA.
      }

      else{
        for(int x = 0; x <6; x++){
          RedLeftON();
          RedRightON();
          BlueLeftON();
          BlueRightON();
          EngineLeftOn();
          EngineRightOn();
          delay(500);
          RedLeftOFF();
          RedRightOFF();
          BlueLeftOFF();
          BlueRightOFF();
          EngineLeftOFF();
          EngineRightOFF();
          delay(500);
        }
        RedLeftOFF();
        RedRightOFF();
        BlueLeftOFF();
        BlueRightOFF();
        EngineLeftOFF();
        EngineRightOFF();
        delay(500);
        //GIRA OS DOIS MOTORES JUNTOS (LIGANDO E DESLIGANDO)
      }
    }
  }
  
  /*digitalWrite(engineLeft, HIGH);   // turn the LED ON (HIGH is the voltage level)
  delay(100);               // wait for a second
  digitalWrite(engineLeft, LOW);    // turn the LED OFF by making the voltage LOW
  delay(100);               // wait for a second*/
}
